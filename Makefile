.PHONY: terraform-fmt

terraform-fmt:
	terraform fmt ./terraform/*.tf
