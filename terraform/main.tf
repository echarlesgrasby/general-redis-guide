// .tfstate will be managed locally for now..
terraform {
  required_providers {
    gitlab = {
      source = "gitlabhq/gitlab"
    }
  }
}

// define vars here. If this grows into a big enough terraform file- i'll split them out into variables.tf
variable "project_name" {
  type    = string
  default = "general-redis-guide"
}

// token is stored in non-version controlled .txt file
provider "gitlab" {
  token = data.local_file.token_val.content
}

resource "gitlab_project" "general-redis-guide-project" {
  name             = var.project_name
  visibility_level = "public"
}

resource "gitlab_project_hook" "webhook_2_discord" {
  project         = gitlab_project.general-redis-guide-project.id
  url             = data.local_file.discord_webhook_url.content
  pipeline_events = true
  push_events     = true

}

// data-block queries
data "local_file" "token_val" {
  filename = "../secs/token.txt"
}

data "local_file" "discord_webhook_url" {
  filename = "../secs/discord-webhook-url.txt"
}
