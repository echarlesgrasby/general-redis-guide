# Getting Started With Redis

![Redis Latest](img/1200px-redis-logo.png)

## Introduction

Redis (Remote Dictionary Server) is a powerful, in-memory datastore. It is designed to serve as a caching system, lookup database, or message broker, depending on developer needs. The following guide is intended to give you an overview of everything that you need to get started using Redis.

> This guide uses Docker to setup a test environment to work with Redis. It assumes that you have Docker installed and are comfortable with basic Docker commands. If you are unfamiliar with Docker, check out the following resources, before proceeding, and then return to to this guide: https://docker-curriculum.com/.

## Why Redis?

Redis is a useful solution for applications that handle volatile data stored in a Key-Value format. Often times this data is written once and read multiple times in a lookup operation, such as in a competitive game leaderboard or a stock ticker. Redis not only provides a mechanism for storing this data, but is also capable of returning values in sub-millisecond response times. This high-performance lookup makes Redis a contender for applications that need to retrieve cached data in real-time.

> Data stored in a Key-Value format has a key that is used to retrieve (or update) that value at a point in time.

## Basic Caching In Redis

Relational databases, while useful for long-term storage, are usually not performant enough. for operations that require fast access to data. To offset this lag in performance- caching is often used. Caching allows a system to persist data outside of a normal database. Redis builds a store of data in-memory

Caching in Redis is an excellent option for applications that require frequent, low-latency, access to data. Any modern distributed system will implement caching for operations that request the same data values on a frequent basis.

## Supported Data Types

* **Binary-safe strings**
  * These are strings that can consist of any sequence of characters: "Hello\r\n"
* **Lists**
  * A list of non-unique elements arranged due to insertion order: [1,2,2,3]
* **Sets**
  * Essentially an unsorted list of unique elements [1,2,3]
* **Hashes**
  * Key-Value pair maps: {"Key": "Value"}
* **Bit Arrays**
  * https://en.wikipedia.org/wiki/Bit_array
* **HyperLogLogs (HLL)**
  * https://en.wikipedia.org/wiki/HyperLogLog
* **Streams**
  * Append-only data collections

> Refer to the Redis docs as they go in much more detail regarding supported data types: https://redis.io/topics/data-types-intro

## Useful Commands

Redis ships with a Command Line Interface used for interacting with the system. The most useful commands include:

* SET
* GET
* EXPIRE
* SETEX
* TTL

> The full command set that Redis supports can be found here, along with that command's runtime efficiency: https://redis.io/commands. The following sections explore these commands in greater detail.

Note that while these commands are useful for managing and experimenting with the system, any applications that you write will most likely use a client to interact with Redis rather than the native CLI. Many official (and unofficial) clients for a large number of languages exist for Redis and can be referenced here: https://redis.io/clients

### Setting Keys

### Getting Values

### Time-To-Live And The `EXPIRE` And `SETEX` Commands

Redis uses a powerful data management concept known as a Time-To-Live (TTL) stamp. The TTL functions as a method for expiring a record when it is no longer needed. A TTL can be set on any record stored in the system via the `EXPIRE` command.

`EXPIRE keyname NUM_OF_SECS`

The current TTL of a record is retrieved by passing the record's key to the `TTL` command.

`TTL keyname`
