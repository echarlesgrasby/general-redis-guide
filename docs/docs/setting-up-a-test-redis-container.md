# Setting Up A Test Redis Container

With the basic functionality of Redis now explained, let's see some of these concepts put to use. To do this, we will setup a test instance of a Redis cache using Docker. Once setup, we will experiment with some of the commands and data types put forth in this tutorial.

To get started, open up your terminal and ensure that Docker is running

Pull the official Docker image from:

```
https://hub.docker.com/_/redis
```

`docker pull redis`

Running the command `docker images` reports that it retrieved the *latest* image of Redis:

![Redis Latest](img/screenshots/docker-images.png)

Start a container from the test image and exec into the container

`docker run -it redis:latest /bin/bash`

Successfully entering the container should return a bash prompt running as the root user

![Redis Docker Exec](img/screenshots/redis-docker-exec.png)
