# general-redis-guide

## Overview

The guides outlined in this repository are intended to give an un-initiated developer enough information to get started using Redis.

I recommend starting [here](./docs/docs/getting-started-with-redis.md).

## Usage

The contents of this repository can be consumed "as-is" in Markdown format- either read natively on GitHub or read, locally, in your favorite text editor.

I am working on integrating `mkdocs` functionality into this repository in parallel with writing the docs. This will allow you to browse the contents of this repo via the `mkdocs` interface on a local web server.

## Purpose

This project serves two purposes: one as a basis for my technical writing portfolio and, two, as a method for getting familiar with Redis. I have no real license for this at the moment. You can use this repo as you wish, but I take/claim _no_ responsibility for any damage you cause to your Redis environment due to actions taken in reliance of information herein.
